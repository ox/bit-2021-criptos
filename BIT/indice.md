-BIT talk-  
... for 45 min about  
"**Criptos, block-chain, Touring-completes and the context where it happens.**"

*bit-2021* by *nechi-group\jsanchez*.

1) ¿Qué es "eso"?  
1.1) White paper: **Bitcoin: A Peer-to-Peer Electronic Cash System**  
1.2) **ethereum.org/en/whitepaper**
2) ¿Qué es Bitcoin?  
2.1) Transactions  
2.2) Timestamp Server  
2.3) Proof-of-Work  
2.4) Network  
2.5) Incentive  
2.6) Reclaiming Disk Space  
2.7) Simplified Payment Verification  
3) Lo de "cripto"  
3.1) SHA: algoritmo de encriptación  
3.1.2) Which-quantum-algorithm-can-break-SHA-256  
3.1.3) Post-quantum cryptography  
3.2) Encriptando "el día a día": Pretty_Good_Privacy  
4) NFT y Tulipanes holandeses.  
4.1) Differences between NFT and FT  
4.2) De la pizza más cara de la historia y otras cosas caras  
4.3) Cuanto vale un BTC  
4.3.1) Cap Btc.  
4.3.2) Cap Eth.
5) Ethereum  
5.1) Surgimiento de los Smart Contracts en Ethereum  
5.2) Solidity  
6) Conclusión  
6.1) Altcoins (=AltTokens), lanzar un ICO propio  
6.2) Contratos inteligentes  
6.3) Infraestructura

# 1) ¿Qué es "eso"?

Aquella mañana de 2013, a un lustro vista, Vitalik, con el *white paper* en estado draft en su portátil, estaba diseñando "**algo más**" sobre **un suelo que apenas si se mantenía visible y estable**. En Octubre de 2008, otro *white paper* había servido de "semilla" porque, en cinco años, desplegó, germinó y floreció ese cierto "algo" que Vitalik usaba como ecosistema para su creación; un "algo" del que Ethereum sería "algo *más*".

Satoshi Nakamoto Institute
https://nakamotoinstitute.org/bitcoin/

## 1.1) White paper: **Bitcoin: A Peer-to-Peer Electronic Cash System**

![](01/bitcoin_white_paper.png)

A purely peer-to-peer version of electronic cash would allow online payments to be sent directly from one party to another without going through a financial institution. Digital signatures provide part of the solution, but the main benefits are lost if a trusted third party is still required to prevent double-spending. We propose a solution to the double-spending problem using a peer-to-peer network.

## 1.2) White paper: **ethereum.org/en/whitepaper**

![](01/ETHEREUM_WHITEPAPER.png)

De aquella mañana, de las semanas previas y posteriores, a un trienio vista, en pp. 87 de [1] de aquel julio de 2013, Tapscott da, indica Wikipedia, buena cuenta. 

[1] Tapscott, Don; Tapscott, Alex (2016).
The Blockchain Revolution: How the Technology Behind Bitcoin is Changing Money, Business, and the World. pp 87

Ethereum was initially described in a white paper by Vitalik Buterin,[4][12]. This introductory paper was originally published in 2013 by Vitalik Buterin, the founder of Ethereum, before the project's launch in 2015. It's worth noting that Ethereum, like many community-driven, open-source software projects, has evolved since its initial inception.

Y si navegamos a https://ethereum.org/en/whitepaper/:

While several years old, we maintain this paper because it continues to serve as a useful reference and an accurate representation of Ethereum and its vision. To learn about the latest developments of Ethereum, and how changes to the NFTprotocol are made, we recommend this guide.

![](01/ETHEREUM.png)

De dónde:

- https://ethereum.org/en/what-is-ethereum/
- https://ethereum.org/en/learn/

# 2) ¿Qué es bitcoin?

![](02/UTXO.png)


Satoshi Nakamoto Institute https://nakamotoinstitute.org/bitcoin/

We have proposed a system for electronic transactions without relying on trust. We started with the usual framework of coins made from digital signatures, which provides strong control of ownership, but is incomplete without a way to prevent double-spending. To solve this, we proposed a peer-to-peer network using proof-of-work to record a public history of transactions that quickly becomes computationally impractical for an attacker to change if honest nodes control a majority of CPU power.

Remarcamos:... **if honest nodes control a majority of CPU power**.

- *Simplified Payment Verification: It is possible to verify payments without running a full network node.* 

- *Combining and Splitting Value: To allow value to be split and combined, transactions contain multiple inputs and outputs.*

- *Privacy: The traditional banking model achieves a level of privacy by limiting access to information to the parties involved and the trusted third party. The necessity to announce all transactions publicly precludes this method, but privacy can still be maintained by breaking the flow of information in another place: by keeping public keys anonymous. The public can see that someone is sending an amount to someone else, but without information linking the transaction to anyone*.

## 2.1) Transactions  
## 2.2) Timestamp Server  
## 2.3) Proof-of-Work  
## 2.4) Network  
## 2.5) Incentive  
## 2.6) Reclaiming Disk Space  
## 2.7) Simplified Payment Verification  

Remarcamos:... **if honest nodes control a majority of CPU power**.

![](02/bitcoin_white_paper_calculations.png)


# 3) Lo de "cripto":  
Lo cierto es que entre 2008 y 2013, bajo los pies de Vitalik, había emergido en la faz de la esfera digital, una red para soportar ese "algo más" que ha llegado a necesitar una aclaración tal que: 

https://twitter.com/vitalikbuterin/status/854271590804140033

 **Is Ethereum really Turing-complete?**

Ethereum, on the other hand, is built as a Turing Complete blockchain. This is important because it needs to understand the agreements which make up smart contracts. By being Turing Complete, Ethereum has the capability to understand and implement any future agreement, even those that have not been thought of yet.

![](05/TURINGCOMPLETEBLOCKCHAINS_Y_VITALIK_ETHEREUM.png)

# 3.1) Which-quantum-algorithm-can-break-SHA-256  
https://www.quora.com/Which-quantum-algorithm-can-break-SHA-256

## 3.1.1) SHA
![](03/sha-256.png)

## 3.1.2) Post-quantum cryptography
From Wikipedia, the free encyclopedia

![](03/post_encript_algoritmos.png)

In cryptography, post-quantum cryptography (sometimes referred to as quantum-proof, quantum-safe or quantum-resistant) refers to cryptographic algorithms (usually public-key algorithms) that are thought to be secure against a cryptanalytic attack by a quantum computer. As of 2021, this is not true for the most popular public-key algorithms, which can be efficiently broken by a sufficiently strong quantum computer.[citation needed] The problem with currently popular algorithms is that their security relies on one of three hard mathematical problems: the integer factorization problem, the discrete logarithm problem or the elliptic-curve discrete logarithm problem. All of these problems can be easily solved on a sufficiently powerful quantum computer running Shor's algorithm.Even though current, publicly known, experimental quantum computers lack processing power to break any real cryptographic algorithm, many cryptographers are designing new algorithms to prepare for a time when quantum computing becomes a threat. This work has gained greater attention from academics and industry through the PQCrypto conference series since 2006 and more recently by several workshops on Quantum Safe Cryptography hosted by the European Telecommunications Standards Institute (ETSI) and the Institute for Quantum Computing.

In contrast to the threat quantum computing poses to current public-key algorithms, most current symmetric cryptographic algorithms and hash functions are considered to be relatively secure against attacks by quantum computers. While the quantum Grover's algorithm does speed up attacks against symmetric ciphers, doubling the key size can effectively block these attacks. Thus post-quantum symmetric cryptography does not need to differ significantly from current symmetric cryptography. See section on symmetric-key approach below.

![](03/sha-256_cuantum.png)

## 3.2) Encriptando "majamente": https://en.wikipedia.org/wiki/Pretty_Good_Privacy

Pretty Good Privacy (PGP) is an encryption program that provides cryptographic privacy and authentication for data communication. PGP is used for signing, encrypting, and decrypting texts, e-mails, files, directories, and whole disk partitions and to increase the security of e-mail communications. Phil Zimmermann developed PGP in 1991.[3]

PGP and similar software follow the OpenPGP, an open standard of PGP encryption software, standard (RFC 4880) for encrypting and decrypting data.

# 4) NFT (y también FTs)

##  4.1) Differences between NFT and FT

![](04/NFT_FT.png)

Native Tokens - Cardano Forum  
https://forum.cardano.org › Developers › Native Tokens

2 Mar 2021 — Hi, thanks for replying. So to conclude, NFT is giving ID to a person or to a thing, while FT is the money that use to pay that item? Correct?


## 4.2) De la pizza más cara de la historia y otras cosas caras
![](04/pizza-buying.png)
¿Cómo se calcula el precio del BTC y cuánto vale el NFT del boceto original y primerísimo del cuerpo verde de Yoda?

![](04/banksy_auction2.png)
¿A cuánto se vende el NFT de esa obra original de Bansky que se transformó en la última subasta y ahora se ha revalorizado, transformado, en una nueva subasta?

![](04/LAPIZZA.png)

Conclusión: el precio de un NFT es "voluble e incierto" en el sentido que depende de oferta/demanda y otros principios generales especulativos. Por sí mismo, un NFT es únicamente un Identificador Único.

![](04/pizzaoriginal.png)

![](04/warhol.png)

![](04/warhol_price.png)

## 4.3) Cuanto vale un BTC.

Los 21 millones, gráfica. Cese de minado.

![](02/Los21millones_.png)

Quedarán operativas las validaciones y el resto de infraestructura. Decremento del coste energético (o, si no decremento, al menos, cese de uno de los actuales consumidores voraces) una vez finalizada la fase de creación de tokens. A partir de ese momento para obtener mayor cantidad de "moneda":

The general unit structure of bitcoins has 1 bitcoin (BTC) equivalent to 1,000 millibitcoins (mBTC), 1,000,000 microbitcoins (μBTC), or 100,000,000 satoshis. While the exact figure is unknown, it is estimated that Satoshi Nakamoto may possess 1 million bitcoins, equivalent to 100,000,000,000,000 satoshis.

## 4.3.1) Cap Btc.
![](04/btc_cap.png)

## 4.3.2) Cap Eth.
![](04/eth_cap.png)


# 5) Ethereum: https://es.cointelegraph.com/explained/what-is-a-smart-contract


![](05/completitud_y_decidibilidad.png)

## 5.1) Surgimiento de los Smart Contracts en Ethereum
Idea de contratos, Zsabo:   https://firstmonday.org/ojs/index.php/fm/article/view/548/469.

![](05/ViewofFormalizingandSecuring.png)

Vitalik Buterin, uno de los principales creadores de Ethereum, observó el concepto de Zsabo y determinó que con la tecnología Blockchain y el sistema descentralizado que había planteado Satoshi Nakamoto, se podrían ejecutar los Contratos Inteligentes dentro de su propuesta, la red de Ethereum.

![](05/VITALIK_TURING.png)

Vitalik consideraba que los Contratos Inteligentes eran aplicaciones complejas que generan que activos digitales se controlen por medio de códigos que apliquen reglas arbritarias. Como creador de Ethereum, propuso una red con un:

“Lenguaje de programación Turing completo incorporado que se puede usar para crear "contratos" que se pueden usar para codificar funciones de transición de estado arbitrarias, permitiendo a los usuarios crear sistemas”.

Los sistemas que creía Vitalik que se podían crear con Contratos Inteligentes eran prácticamente ilimitados. Para el momento de la creación de Ethereum, planteaba que estos programas podían emplearse en tres tipos de aplicaciones: Aplicaciones financieras, Aplicaciones semi-financieras y Aplicaciones no financieras.

Entre esas aplicaciones, **podemos encontrarnos usos para Sistemas de identidad y Reputación, Resguardo de archivos de forma descentralizada, Organizaciones Autónomas Descentralizadas (DAO), Mercado Descentralizado Blockchain, Apuestas Peer-to-Peer, Sistemas de Escrow con múltiples garantes y muchas otras características**. La verdad es que las aplicaciones planteadas por Vitalik fueron tan solo algunas de las posibilidades que podían surgir dentro del mercado.

## 5.2) Solidity
**¿Cómo se hacen los Contratos Inteligentes en Ethereum? ¿Es igual a escribir un documento normal?**
La realidad es que no se escriben igual, debemos de recordar que los Contratos Inteligentes son programas computacionales y requieren ser programados como tal. El código o lenguaje que se utiliza para escribir estos contratos dentro de la red de Ethereum se denomina “Solidity”.

Este programa es **un lenguaje informático algo complejo que permite resolver complejos problemas informáticos, así como tener la posibilidad de ejecutar tareas por medio de instrucciones. A este tipo de códigos se les denomina lenguaje completo de Turing** porqué está planteado para procesos con un alto poder de cómputo.

En cierta forma, la implementación de este lenguaje genera que la red de Ethereum sea más compleja que la red de Bitcoin y su lenguaje “Bitcoin Transaction Language”. Ello es positivo porque **permite plasmar elementos más complejos dentro de la red Blockchain como son aplicaciones descentralizadas y los propios Contratos Inteligentes**, pero también genera que se consuma mayor poder computacional para realizar estas operaciones y ello implica un aumento en los costos para su utilización.

# 6) Conclusión
El "error del inversor" de invertir en Tesla o invertir en Ethereum. La inversión en Tesla es "algo": si la cotización de Tesla sube tus acciones suben; la de Ethereum tiene "algo más" que la de "bitcoin",...

## 6.1) Altcoins, lanzar un ICO propio

![](06/tokendeICO.png)

Que existan tantas Alcoins indica la "facilidad" para crear ecosistemas basados en cadena de bloques.

Por ejemplo:
https://es.cointelegraph.com/ico-101/what-is-an-ico-token-and-how-does-it-work
https://es.cointelegraph.com/ico-101/how-to-launch-an-ico-a-detailed-guide

Cuando se trata de plataformas de cadenas de bloques utilizadas para emitir tokens para las ICO, el mercado realmente no es tan competitivo. Según los datos de ICOWatchList, el 82,46 por ciento de los proyectos utiliza la plataforma Ethereum, mientras que el resto opta por desarrollar sus propias plataformas de cadenas de bloques personalizadas. Existen, por supuesto, otros servicios como Waves, Stratis o Hyperledger, pero incluso su cuota de mercado combinada es realmente pequeña en comparación con lo que puede hacer Ethereum.

![](06/ALTCOINS.png)

La creación del token en sí es un proceso relativamente fácil, especialmente cuando se trata de los tokens de Ethereum ERC20. El sitio web de Ethereum incluso enumera el código que necesitarás usar. Sin embargo, personalizar ciertos aspectos para organizar la venta de esos tokens al público puede ser un poco más difícil, así que si no eres exactamente un experto en tecnología, considera contratar a un profesional para que lo haga por ti.

![](06/creartokeinico.png)

Consulta las mejores plataformas para lanzas una ICO: Un repaso por las mejores plataformas para crear contratos inteligentes

## 6.2) Contratos inteligentes

Smart contracts combine protocols with user interfaces to formalize and secure relationships over computer networks. Objectives and principles for the design of these systems are derived from legal principles, economic theory, and theories of reliable and secure protocols. Similarities and differences between smart contracts and traditional business procedures based on written contracts, controls, and static forms are discussed. By using cryptographic and other security mechanisms, we can secure many algorithmically specifiable relationships from breach by principals, and from eavesdropping or malicious interference by third parties, up to considerations of time, user interface, and completeness of the algorithmic specification. This article discusses protocols with application in important contracting areas, including credit, content rights management, payment systems, and contracts with bearer.

![](06/Vitalik_RichState.png)

## 6.3) Infraestructura

https://www.hyperledger.org/

![](01/HYPERLEDGER.png)

- https://www.microsoft.com/en-us/research/blog/leveraging-blockchain-to-make-machine-learning-models-more-accessible/

![](06/leveraging-blockchain-to-make.png)

Leveraging blockchain technology allows us to do two things that are integral to the success of the framework: offer participants a level of trust and security and reliably execute an incentive-based system to encourage participants to contribute data that will help improve a model’s performance.