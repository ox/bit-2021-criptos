# Bit 2021 Criptos

-BIT talk-  
... for 45 min about  
"**Criptos, block-chain, Touring-completes and the context where it happens.**"

1) ¿Qué es "eso"?  
1.1) White paper: **Bitcoin: A Peer-to-Peer Electronic Cash System**  
1.2) **ethereum.org/en/whitepaper**
2) ¿Qué es Bitcoin?  
2.1) Transactions  
2.2) Timestamp Server  
2.3) Proof-of-Work  
2.4) Network  
2.5) Incentive  
2.6) Reclaiming Disk Space  
2.7) Simplified Payment Verification  
3) Lo de "cripto"  
3.1) SHA: algoritmo de encriptación  
3.1.2) Which-quantum-algorithm-can-break-SHA-256  
3.1.3) Post-quantum cryptography  
3.2) Encriptando "el día a día": Pretty_Good_Privacy  
4) NFT y Tulipanes holandeses.  
4.1) Differences between NFT and FT  
4.2) De la pizza más cara de la historia y otras cosas caras  
4.3) Cuanto vale un BTC  
4.3.1) Cap Btc.  
4.3.2) Cap Eth.
5) Ethereum  
5.1) Surgimiento de los Smart Contracts en Ethereum  
5.2) Solidity  
6) Conclusión  
6.1) Altcoins (=AltTokens), lanzar un ICO propio  
6.2) Contratos inteligentes  
6.3) Infraestructura
